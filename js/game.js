var Classification = {
 	"SHAPES": "shapes",
	"COLORS": "colors",
}

var Group = {
 	"RED": "red",
	"BLUE": "blue",
	"YELLOW": "yellow",
	"GREEN": "green",

	"CIRCLE": "circle",
	"SQUARE": "square",
	"RECTANGLE": "rectangle",
	"TRIANGLE": "triangle"
}

var Canvas = function($my_canvas, my_group) {
	this.canvas = $my_canvas;
	this.group = my_group;
	this.onPlace = false;
}

var Game = function($objects_container) {

	var self = this;
	var container = $objects_container;
	var boxesContainer;

	var hasError = false;
	var levelImg = null;
	var classificationImg = null;

	var level = null;
	var classification = null;

	var img_boxes_height = "600";
	var img_boxes_width = "750";
	var targets_height = "500";
	var targets_width = "500";

	var boxes_height = 350;
	var boxes_width = 400;

	var canvas_boxes = [];
	var canvas_targets = [];

	this.setClassification = function(_classification) {
		classification = _classification;
	};

	this.getClassification = function() {
		return classification;
	};

	this.setLevel = function(_level) {
		level = _level;
	};

	this.getLevel = function() {
		return level;
	};

	function showHeader() {
		var headerContainer = $("<div></div>").attr("align", "left");
		if (level == 1) {
			levelImg = $("<img></img>").attr("src", "img/level-1.png").attr("height", "50");
		}
		else if (level == 2) {
			levelImg = $("<img></img>").attr("src", "img/level-2.png").attr("height", "50");
		}
		else if (level == 3) {
			levelImg = $("<img></img>").attr("src", "img/level-3.png").attr("height", "50");
		}

		if (classification == Classification.COLORS) {
			classificationImg = $("<img></img>").attr("src", "img/color-classification.jpg")
			.attr("height", "50").attr("width", "50");
		}
		else if (classification == Classification.SHAPES) {
			classificationImg = $("<img></img>").attr("src", "img/shape-classification2.png")
			.attr("height", "50").attr("width", "50");
		}

		headerContainer.append(levelImg);
		headerContainer.append(classificationImg);
		container.append(headerContainer);
	};

	function createBoxCanvas(img_name, group) {
		var canvas = $("<canvas></canvas>").addClass("canvas-boxes").css("width", boxes_width).css("height", boxes_height);
		var canvasObj = new Canvas(canvas, group);
		canvas_boxes.push(canvasObj);
		boxesContainer.append(canvas);
		canvas[0].width = boxes_width;
		canvas[0].height = boxes_height;
		drawImageBox(canvas, img_name);
	}

	function showBoxesCanvas() {
		boxesContainer = $("<div></div>").attr("align", "center").attr("padding-top", "1%").attr("display", "inline-block").attr("margin", "20px");
		container.append(boxesContainer);

		if (classification == Classification.COLORS) {
			createBoxCanvas("blue_box.png", Group.BLUE);
			createBoxCanvas("red_box.png", Group.RED);

			if (level >= 2) {
				createBoxCanvas("yellow_box.png", Group.YELLOW);
			}
			if (level >= 3) {
				createBoxCanvas("green_box.png", Group.GREEN);
			}
		} else {
			createBoxCanvas("square_box.png", Group.SQUARE);
			createBoxCanvas("circle_box.png", Group.CIRCLE);

			if (level >= 2) {
				createBoxCanvas("rectangle_box.png", Group.RECTANGLE);
			}
		}
	};

	function createTargetCanvas(img, group) {
		var canvas = $("<canvas></canvas>")
						.addClass("canvas-targets")
						.css("width", 250)
						.css("height", 250);
					canvas[0].width = 200;
					canvas[0].height = 200;

	    var canvasObj = new Canvas(canvas, group);					
	    canvas_targets.push(canvasObj);
		container.append(canvas);
		drawImage(canvas, img);
		self.addCollision(canvas);
		self.addMovementToCanvas(canvas[0]);		
	}

	function showTargetsCanvas() {
		if (classification == Classification.SHAPES) {
			if (level == 1) {
				createTargetCanvas("stones.jpg", Group.CIRCLE);
				createTargetCanvas("target2.jpg", Group.SQUARE);
				createTargetCanvas("target3.jpg", Group.CIRCLE);
				createTargetCanvas("target4.jpg", Group.SQUARE);
				createTargetCanvas("target6.jpg", Group.CIRCLE);
				createTargetCanvas("tracker.jpg", Group.SQUARE);
			} else {
				createTargetCanvas("stones.jpg", Group.CIRCLE);
				createTargetCanvas("target2.jpg", Group.SQUARE);
				createTargetCanvas("target3.jpg", Group.SQUARE);
				createTargetCanvas("target4.jpg", Group.RECTANGLE);
				createTargetCanvas("target6.jpg", Group.CIRCLE);
				createTargetCanvas("tracker.jpg", Group.RECTANGLE);
			} 
		} else {
			if (level == 1) {
				createTargetCanvas("stones.jpg", Group.RED);
				createTargetCanvas("target2.jpg", Group.RED);
				createTargetCanvas("target3.jpg", Group.BLUE);
				createTargetCanvas("target4.jpg", Group.BLUE);
				createTargetCanvas("target6.jpg", Group.RED);
				createTargetCanvas("tracker.jpg", Group.BLUE);
			} else if (level == 2) {
				createTargetCanvas("stones.jpg", Group.RED);
				createTargetCanvas("target2.jpg", Group.RED);
				createTargetCanvas("target3.jpg", Group.YELLOW);
				createTargetCanvas("target4.jpg", Group.BLUE);
				createTargetCanvas("target6.jpg", Group.YELLOW);
				createTargetCanvas("tracker.jpg", Group.BLUE);
			} else {
				createTargetCanvas("stones.jpg", Group.GREEN);
				createTargetCanvas("target2.jpg", Group.GREEN);
				createTargetCanvas("target3.jpg", Group.BLUE);
				createTargetCanvas("target4.jpg", Group.YELLOW);
				createTargetCanvas("target6.jpg", Group.RED);
				createTargetCanvas("tracker.jpg", Group.YELLOW);
			}
		}
	}

	this.begin = function() {
		container.empty();
		showHeader();
		showBoxesCanvas();
		showTargetsCanvas();
	};

	function drawImage(canvas, name) {
		var render = canvas[0].getContext("2d");
		var img = new Image();
		img.onload = function () {
	    	render.drawImage(img, 0, 0, targets_width, targets_height, 0, 0, canvas[0].width, canvas[0].height);
		}
		img.src = "img/targets/" + name;
	};

	function drawImageBox(canvas, name) {
		var render = canvas[0].getContext("2d");
		var img = new Image();
		img.onload = function () {
	    	render.drawImage(img, 0, 0, img_boxes_width, img_boxes_height, 0, 0, canvas[0].width, canvas[0].height);
		}
		img.src = "img/" + name;
	};

	this.addMovementToCanvas = function(value) {
	    var hammertime = new Hammer(value, {domEvents:true});
	    var canvasTopPosition = $(value).offset().top;
	    hammertime.on('pan', function(ev) {
	       var x = ev.center.x - (value.width);
	       if (x > container.offset().left && x < container.offset().left + container.width() - value.width) {
	         value.style.left = x+"px";
	       }

	       value.style.top = ev.center.y+"px";
     	});
  	};

  	this.hasTargetOnWrongPlace = function() {
  		hasError = false;
  		canvas_targets.forEach(function(canvasObj) {
  			if (canvasObj.onPlace == false)
  				hasError = true;
  		});
  		return hasError;
  	};

  	this.addCollision = function(canvasw) {
		$(canvasw).on("click touchend", function(event) {
			
			$(".canvas-targets").each(function(index) {
				var tCanvasObj = canvas_targets[index];
				var canvas = tCanvasObj.canvas;

				var targetPosX = canvas[0].offsetLeft;
				var targetPosY = canvas[0].offsetTop;
				var targetWidth = parseInt(canvas[0].style.width, 10);
				var targetHeight = parseInt(canvas[0].style.height, 10);
				
				$(".canvas-boxes").each(function(index) {
					var bCanvasObj = canvas_boxes[index];
				 	var boxCanvas = bCanvasObj.canvas;
				 	var boxPosX = boxCanvas[0].offsetLeft;
					var boxPosY = boxCanvas[0].offsetTop;	

					var boxWidth = parseInt(boxCanvas[0].style.width, 10);
					var boxHeight = parseInt(boxCanvas[0].style.height, 10);

					if(targetPosX + targetWidth > boxPosX && targetPosX < boxPosX + boxWidth && 
					  targetPosY + targetHeight > boxPosY && targetPosY < boxPosY + boxHeight) {
					  	if (tCanvasObj.group == bCanvasObj.group) {
							//canvas[0].style.display = "none";
							tCanvasObj.onPlace = true;
						} else {
							tCanvasObj.onPlace = false;
						}
				 	}

				 });
			});
		});	
	};
};